// import 'imagesloaded';
import './jquery-global.js';
import './imagesloaded.pkgd.min.js';
import './jquery.deorphan.js';
require('velocity-animate');
// import '../../node_modules/kute.js/kute.js';
// import '../../node_modules/kute.js/kute-svg.js';

(function() {

    var Site = {
        vars: {
            mode: undefined,
            maxBgMovement: undefined,
            windowW: undefined,
            isSliderActive: undefined,
            isAnimating: false
        },

        init: function () {
            Site.kodohouse();
            Site.initVars();
            Site.handleResize();
            Site.switcher.init();
            Site.swipers.init();
            // Site.desktop.init();
            Site.rollTheCan();
            Site.orphans();

            Site.vars.windowW >= 992 ? Site.desktop.init() : Site.mobile.init();
        },

        kodohouse: function () {
            console.log('Designed by feldman.studio http://feldman.studio/');
            console.log('Coded by kodo.house http://kodo.house/');
        },

        loader: function () {
            Site.init();

            $('.js-loader-top').velocity(
                { height: '60%' },
                { duration: 15000 },
                'easeOutQuint'
            );

            $('#site').imagesLoaded(
                { background: '.js-splash-line, .js-can, .js-slider-slide' }
            )
            .always( function(instance) {
                $('.js-loader-top').velocity('stop').velocity(
                    { height: '90%' },
                    { duration: 1000 },
                    'easeInQuint'
                );
                setTimeout(function () {
                    $('#site').addClass('is-loaded');
                    setTimeout(function () {
                        $('.js-loader').hide();
                    }, 1500);
                }, 1000);
                setTimeout(function () {
                    $('.js-slider').addClass('is-scrollable');
                }, 3000);
            })
            .done( function(instance) {
                // console.log('all images successfully loaded');
            })
            .fail( function() {
                // console.log('all images loaded, at least one is broken');
            })
            .progress( function(instance, image) {
                var result = image.isLoaded ? 'loaded' : 'broken';
                // console.log( 'image is ' + result + ' for ' + image.img.src );
            });
        },

        orphans: function () {
            $('p').deOrphan();
        },

        initVars: function () {
            Site.vars.windowW = $(window).width();
            Site.vars.windowH = $(window).height();
            Site.vars.maxBgMovement = parseInt(Site.vars.windowW * 0.1171875);
            Site.vars.windowHalfW = Site.vars.windowW / 2;
        },

        handleResize: function () {
            var breakpoint = window.matchMedia("(min-width: 992px)");

            $(window).on('resize', function () {
                Site.initVars();
                $('.js-splash-line').css('transform', 'translate3d(0, 0, 0)');
            });

            function breakpointChecker() {
                location.reload();
                if (breakpoint.matches === true) {
                    Site.mobile.destroy();
                    Site.desktop.init();
                } else if ( breakpoint.matches === false ) {
                    Site.desktop.destroy();
                    Site.mobile.init();
                }
            }

            breakpoint.addListener(breakpointChecker);
        },

        switcher: {
            init: function () {
                this.addEvents();
            },

            addEvents: function () {
                $('.js-switcher').on('click', this.switchIt);
            },

            switchIt: function (e) {
                $('body').toggleClass('is-day is-night');
                $(this).toggleClass('switcher--day switcher--night');

                if (Site.swipers.daySlider !== undefined) {
                    Site.swipers.daySlider.slideTo(0);
                }
                if (Site.swipers.nightSlider !== undefined) {
                    Site.swipers.nightSlider.slideTo(0);
                }
            }
        },

        desktop: {
            settings: {
                sliderSpeed: 2001
            },

            tweens: {
                dayTween1: undefined,
                dayTween2: undefined,
                dayTween3: undefined,
                dayTween4: undefined,
                nightTween1: undefined,
                nightTween2: undefined,
                nightTween3: undefined,
                nightTween4: undefined
            },

            init: function () {
                Site.desktop.transformBackground();
                this.addEvents();
                this.mouseEvents();
                // this.startWaves();
                // this.animateArrows();
            },

            destroy: function () {
                $('.js-splash').off('mousemove');
                $('.js-slider').removeClass('is-revealed is-active');
                $('.js-slide-wipe').removeClass('is-active is-wiped is-wiped-reverse');
                Site.desktop.animateBackToSplash();
            },

            addEvents: function () {
                $('.js-scroll-btn').on('click', this.animateToSlider);

                $('.js-arrow-next').on('click', function (e) {
                    e.preventDefault();
                    if (Site.vars.isAnimating) return;
                    Site.waitForAnimations(Site.desktop.settings.sliderSpeed);
                    var $slider = $(this).siblings('.js-swiper-desktop');
                    Site.desktop.swipeNextSlide($slider);
                });

                $('.js-arrow-prev').on('click', function (e) {
                    e.preventDefault();
                    if (Site.vars.isAnimating) return;
                    Site.waitForAnimations(Site.desktop.settings.sliderSpeed);
                    var $slider = $(this).siblings('.js-swiper-desktop');
                    Site.desktop.swipePrevSlide($slider);
                });

                var indicator = new WheelIndicator({
                   elem: document.querySelector('.js-slider-day'),
                   callback: function (e) {
                       if (Site.vars.isAnimating) return;
                       if (e.direction == 'up') {
                           Site.desktop.animateBackToSplash();
                       } else {
                           Site.desktop.animateToSlider();
                       }
                   }
               });

               var indicator2 = new WheelIndicator({
                  elem: document.querySelector('.js-slider-night'),
                  callback: function (e) {
                      if (Site.vars.isAnimating) return;
                      if (e.direction == 'up') {
                          Site.desktop.animateBackToSplash();
                      } else {
                          Site.desktop.animateToSlider();
                      }
                  }
              });
            },

            startWaves: function () {

                if (Site.desktop.tweens.dayTween1 !== undefined) {
                    Site.desktop.resumeWaves();
                    return;
                }

                Site.desktop.tweens.dayTween1 = KUTE.fromTo('#day-wave-left-path-1', {path: '#day-wave-left-path-1' }, { path: '#day-wave-left-path-2' },
                    { complete: function() { Site.desktop.tweens.dayTween2.start() }, duration: 1500 }
                ).start();

                Site.desktop.tweens.dayTween2 = KUTE.fromTo('#day-wave-left-path-1', {path: '#day-wave-left-path-2' }, { path: '#day-wave-left-path-1' },
                    { complete: function() { Site.desktop.tweens.dayTween1.start() }, duration: 1500 }
                );

                Site.desktop.tweens.dayTween3 = KUTE.fromTo('#day-wave-right-path-1', {path: '#day-wave-right-path-1' }, { path: '#day-wave-right-path-2' },
                    { complete: function() { Site.desktop.tweens.dayTween4.start() }, duration: 1500 }
                ).start();

                Site.desktop.tweens.dayTween4 = KUTE.fromTo('#day-wave-right-path-1', {path: '#day-wave-right-path-2' }, { path: '#day-wave-right-path-1' },
                    { complete: function() { Site.desktop.tweens.dayTween3.start() }, duration: 1500 }
                );

                Site.desktop.tweens.nightTween1 = KUTE.fromTo('#night-wave-left-path-1', {path: '#night-wave-left-path-1' }, { path: '#night-wave-left-path-2' },
                    { complete: function() { Site.desktop.tweens.nightTween2.start() }, duration: 1500 }
                ).start();

                Site.desktop.tweens.nightTween2 = KUTE.fromTo('#night-wave-left-path-1', {path: '#night-wave-left-path-2' }, { path: '#night-wave-left-path-1' },
                    { complete: function() { Site.desktop.tweens.nightTween1.start() }, duration: 1500 }
                );

                Site.desktop.tweens.nightTween3 = KUTE.fromTo('#night-wave-right-path-1', {path: '#night-wave-right-path-1' }, { path: '#night-wave-right-path-2' },
                    { complete: function() { Site.desktop.tweens.nightTween4.start() }, duration: 1500 }
                ).start();

                Site.desktop.tweens.nightTween4 = KUTE.fromTo('#night-wave-right-path-1', {path: '#night-wave-right-path-2' }, { path: '#night-wave-right-path-1' },
                    { complete: function() { Site.desktop.tweens.nightTween3.start() }, duration: 1500 }
                );
            },

            pauseWaves: function () {
                Site.desktop.tweens.dayTween1.pause();
                Site.desktop.tweens.dayTween2.pause();
                Site.desktop.tweens.dayTween3.pause();
                Site.desktop.tweens.dayTween4.pause();

                Site.desktop.tweens.nightTween1.pause();
                Site.desktop.tweens.nightTween2.pause();
                Site.desktop.tweens.nightTween3.pause();
                Site.desktop.tweens.nightTween4.pause();
            },

            resumeWaves: function () {
                Site.desktop.tweens.dayTween1.play();
                Site.desktop.tweens.dayTween2.play();
                Site.desktop.tweens.dayTween3.play();
                Site.desktop.tweens.dayTween4.play();

                Site.desktop.tweens.nightTween1.play();
                Site.desktop.tweens.nightTween2.play();
                Site.desktop.tweens.nightTween3.play();
                Site.desktop.tweens.nightTween4.play();
            },

            swipeNextSlide: function ($slider) {
                var $activeSlide = $slider.find('.js-slide-wipe.is-active');
                var $nextSlide = $slider.find('.js-slide-wipe').eq($activeSlide.index());
                $nextSlide.addClass('is-wiped');

                setTimeout(function () {
                    $slider.closest('.js-slider').find('.js-pagination-bullet').removeClass('is-active');
                    $slider.closest('.js-slider').find('.js-pagination-bullet').eq($nextSlide.data('slide-index')).addClass('is-active');
                }, Site.desktop.settings.sliderSpeed * 0.5)

                setTimeout(function () {
                    $nextSlide.find('.js-slide-contents').addClass('is-active');
                }, Site.desktop.settings.sliderSpeed * 0.7);

                setTimeout(function () {
                    $activeSlide.removeClass('is-active is-wiped');
                    $activeSlide.find('.js-slide-contents').removeClass('is-active');
                    $nextSlide.addClass('is-active');
                    $slider.find('.js-slide-wipe').eq(0).appendTo($slider);
                }, Site.desktop.settings.sliderSpeed);
            },

            swipePrevSlide: function ($slider) {
                var $activeSlide = $slider.find('.js-slide-wipe.is-active');
                var $prevSlide = $slider.find('.js-slide-wipe').last().insertAfter($slider.find('.js-splash'));

                $prevSlide.addClass('is-wiped is-active').removeClass('is-wiped-reverse');
                $activeSlide.removeClass('is-wiped is-active').addClass('is-wiped-reverse');

                setTimeout(function () {
                    $slider.closest('.js-slider').find('.js-pagination-bullet').removeClass('is-active');
                    $slider.closest('.js-slider').find('.js-pagination-bullet').eq($prevSlide.data('slide-index')).addClass('is-active');
                }, Site.desktop.settings.sliderSpeed * 0.5);

                setTimeout(function () {
                    $prevSlide.find('.js-slide-contents').addClass('is-active');
                }, Site.desktop.settings.sliderSpeed * 0.6);

                setTimeout(function () {
                    $activeSlide.removeClass('is-wiped-reverse');
                    $activeSlide.find('.js-slide-contents').removeClass('is-active');
                }, Site.desktop.settings.sliderSpeed);
            },

            transformBackground: function () {
                var startPosW;
                var $splashLine = $('.js-splash-line');

                function moveBackground(movementW) {
                    if (movementW < -Site.vars.maxBgMovement ) {
                        movementW = -Site.vars.maxBgMovement ;
                    }
                    if (movementW > Site.vars.maxBgMovement ) {
                        movementW = Site.vars.maxBgMovement ;
                    }
                    $splashLine.css('transform', 'translate3d(' + movementW + 'px, 0, 0)');
                }

                $('.js-splash').on('mousemove', function (e) {
                    startPosW = startPosW || Site.vars.windowHalfW;
                    var movementW = (startPosW - e.clientX) / 4;
                    moveBackground(movementW);
                });
            },

            changeSlides: function () {

            },

            animateToSlider: function (e) {
                if (e) e.preventDefault();
                if ($('.slider').hasClass('is-revealed') || !$('.js-slider').hasClass('is-scrollable')) return;

                Site.desktop.startWaves();
                Site.waitForAnimations(3001);
                $('.js-slider').addClass('is-revealed');
                setTimeout(function () {
                    $('.js-slider').addClass('is-active');
                }, 3001);

                // 1. Fade away bg line and text
                $('.js-fade-away').velocity({
                    blur: '5px',
                    opacity: '0',
                    scale: '0.93'
                }, {
                    duration: 1500,
                    easing: 'easeOutQuint'
                });

                // 2. Move down the can and button
                $('.js-move-down').velocity({
                    translateY: '63vh'
                }, {
                    duration: 1500,
                    delay: 200,
                    easing: 'easeOutExpo'
                });

                // 3. Move up center part bg
                $('.js-move-up').velocity({
                    translateY: '-110vh',
                    skewY: '7deg',
                    scaleY: '1.1'
                }, {
                    duration: 600,
                    delay: 300,
                    easing: 'easeInSine'
                });

                // 4a. Rotate left part bg
                $('.js-rotate-left').velocity({
                    rotateZ: '-55deg'
                }, {
                    duration: 1500,
                    delay: 1500,
                    easing: 'easeInOutQuart'
                });

                // 4b. Rotate right part bg
                $('.js-rotate-right').velocity({
                    rotateZ: '55deg'
                }, {
                    duration: 1500,
                    delay: 1500,
                    easing: 'easeInOutQuart'
                });

                // 5. Fade in social + arrows
                $('.js-fade-social, .js-slider-pagination').velocity('fadeIn', {
                    duration: 300,
                    delay: 2500,
                    easing: 'easeInOutQuad'
                });

                // 6. Slide left contact
                $('.js-fade-contact').velocity({
                    translateX: '-8px',
                    opacity: '1'
                }, {
                    duration: 250,
                    delay: 2500,
                    easing: 'easeOutQuad',
                    display: 'block'
                });

                // 7. Slide right note
                $('.js-fade-note').velocity({
                    translateX: '8px',
                    opacity: '1'
                }, {
                    duration: 250,
                    delay: 2500,
                    easing: 'easeOutQuad',
                    display: 'block'
                });

                // 8. Fade in arrows
                $('.js-arrow').velocity({
                    opacity: '1'
                }, {
                    duration: 400,
                    delay: 2500,
                    easing: 'easeInOutQuad',
                    display: 'inline-block'
                });

                setTimeout(function () {
                    $('.js-slider').find('.js-slide-wipe.is-active .js-slide-contents').addClass('is-active');
                }, 2900);
            },

            animateBackToSplash: function (e) {
                if (e) e.preventDefault();
                if (!$('.js-slider').hasClass('is-revealed')) return;

                Site.waitForAnimations(2401);
                $('.js-slider').removeClass('is-revealed is-active');

                // 1. Fade out arrows
                $('.js-arrow, .js-fade-social, .js-slider-pagination').velocity({
                    opacity: '0'
                }, {
                    duration: 1500,
                    easing: 'easeOutQuart'
                    // display: 'none'
                });

                // 1. Rotate back bg's
                $('.js-rotate-left, .js-rotate-right').velocity({
                    rotateZ: '0deg'
                }, {
                    duration: 1500,
                    easing: 'easeInOutQuart'
                });

                // 6. Slide left contact
                $('.js-fade-contact').velocity({
                    translateX: '0',
                    opacity: '0'
                }, {
                    duration: 400,
                    easing: 'easeInQuad',
                    display: 'none'
                });

                // 7. Slide right note
                $('.js-fade-note').velocity({
                    translateX: '0',
                    opacity: '0'
                }, {
                    duration: 400,
                    easing: 'easeInQuad',
                    display: 'none'
                });

                // 2. Move up can
                $('.js-move-down').velocity({
                    translateY: '0vh'
                }, {
                    duration: 1000,
                    delay: 1400,
                    easing: 'easeInQuad'
                });

                // 3. Move up center part bg
                $('.js-move-up').velocity({
                    translateY: '0vh',
                    skewY: '7deg',
                    scaleY: '1.1'
                }, {
                    duration: 500,
                    delay: 1500,
                    easing: 'easeInSine'
                });

                $('.js-fade-away').velocity({
                    blur: '0',
                    opacity: '1',
                    scale: '1'
                }, {
                    duration: 700,
                    delay: 1700,
                    easing: 'easeInQuad'
                });

                setTimeout(function () {
                    Site.desktop.pauseWaves();
                }, 2000)

                // setTimeout(function () {
                    $('.js-slider').find('.js-slide-wipe.is-active .js-slide-contents').removeClass('is-active');
                // }, 3000);
            },

            mouseEvents: function () {
                var mouse = {
                    x: 0,
                    y: 0,
                    sx: 0,
                    sy: 0,
                    scx: 0,
                    scy: 0
                }

                $(document).on('mousemove', function (e) {
                    var x = e.pageX;
                    var y = e.pageY;
                    var cx = (x - Site.vars.windowW / 2) / Site.vars.windowW / 2;
                    var cy = (y - Site.vars.windowH / 2) / Site.vars.windowH / 2;
                    var fx = x / Site.vars.windowW;
                    var fy = y / Site.vars.windowH;
                    mouse.sx = x;
                    mouse.sy = y;
                    mouse.scx = cx;
                    mouse.scy = cy;
                })

                $('.js-sticky').on('mousemove', function (e) {
                    var it = this;
                    var w = this.clientWidth;
                    var h = this.clientHeight;
                    var rect = this.getBoundingClientRect();
                    var x = rect.left;
                    var y = rect.top;
                    var dx = (mouse.sx - x) / w - 0.5;
                    var dy = (mouse.sy - y) / h - 0.5;

                    TweenLite.to(it, 0.33, {
                        x: dx * w * 0.5,
                        y: dy * h * 0.75,
                    });
                });

                $('.js-sticky').on('mouseleave', function (e) {
                    var it = this;
                    TweenLite.to(it, 0.4, {
                        x: 0,
                        y: 0,
                    });
                });
            }
        },

        mobile: {
            init: function () {
                Site.swipers.enableSliders();
                this.addEvents();
            },

            addEvents: function () {
                $('.js-scroll-btn').on('click', function (e) {
                    e.preventDefault();
                    if ($('body').hasClass('is-day')) {
                        Site.swipers.daySlider.slideTo(1);
                    } else if ($('body').hasClass('is-night')) {
                        Site.swipers.nightSlider.slideTo(1);
                    }
                });
            },

            destroy: function () {
                $('.js-scroll-btn').off('click');
                if (Site.swipers.daySlider !== undefined) {
                    Site.swipers.daySlider.destroy();
                    Site.swipers.daySlider = undefined;
                }
                if (Site.swipers.nightSlider !== undefined) {
                    Site.swipers.nightSlider.destroy();
                    Site.swipers.nightSlider = undefined;
                }
            }
        },

        swipers: {
            daySlider: undefined,
            nightSlider: undefined,

            init: function () {
                this.addEvents();
            },

            addEvents: function () {

            },

            createSlider: function (selector) {
                var slider = new Swiper(selector, {
                    direction: 'vertical',
                    loop: false,
                    speed: 700,
                    followFinger: false,
                    mousewheel: false,
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets'
                    },
                    on: {
                        slideChangeTransitionStart: function () {
                            slider.isAnimating = true;
                        },
                        slideChangeTransitionEnd: function () {
                            slider.isAnimating = false;
                        },
                        slidePrevTransitionStart: function () {
                            if (this.activeIndex === 0) {
                                $('.js-hide-on-splash-mobile').addClass('hide-on-splash-mobile');
                            }
                        },
                        slideNextTransitionEnd: function () {
                            if (this.activeIndex === 1) {
                                $('.js-hide-on-splash-mobile').removeClass('hide-on-splash-mobile');
                            }
                        },
                        beforeDestroy: function () {
                            $('.js-hide-on-splash-mobile').addClass('hide-on-splash-mobile');
                        }
                    }
                });

                var indicator = new WheelIndicator({
                   elem: document.querySelector(selector),
                   callback: function (e) {
                       if (slider.isAnimating || slider.destroyed) {
                           return;
                       }
                       if (e.direction == 'up') {
                           slider.slidePrev();
                       } else {
                           slider.slideNext();
                       }
                   }
               });
               return slider;
           },

           enableSliders: function () {
               Site.swipers.daySlider = Site.swipers.createSlider('.js-slider-day');
               Site.swipers.nightSlider = Site.swipers.createSlider('.js-slider-night');
           }
        },

        rollTheCan: function () {
            var frames = 71;
            var frameWidth = Math.round(window.innerWidth / frames);
            var xIndex;
            var timeout;

            function updatePosition(x) {
                if (xIndex != x) {
                    xIndex = x;
                    $('.js-can').attr('data-position', xIndex);
                }
            }

            function animateOnDeviceMove(value) {
                clearTimeout(timeout);
                xIndex = Math.abs(parseInt(value / 2.8));
                if (Math.abs(value) > 79) return;
                if (value > 0) {
                    xIndex = parseInt(xIndex + frames / 2);
                } else {
                    xIndex = parseInt(frames / 2 - xIndex);
                }
                $('.js-can').attr('data-position', xIndex);
            } // 

            document.onmousemove = function(e) {
                clearTimeout(timeout);
                timeout = setTimeout(()=>  updatePosition(Math.round(e.clientX / frameWidth)), 1 )
            }

            // window.addEventListener('deviceorientation', function(e) {
            //     animateOnDeviceMove(e.gamma);
            // });

            if ($(window).width() > 992) {
                window.addEventListener('deviceorientation', function(e) {
                    animateOnDeviceMove(e.beta);
                });
            } else {
                window.addEventListener('deviceorientation', function(e) {
                    animateOnDeviceMove(e.gamma);
                });
            }

            $(window).on('resize', function () {
                frameWidth = Math.round($(window).width() / frames);
            });
        },

        waitForAnimations: function (duration) {
            Site.vars.isAnimating = true;
            $('*').css('pointer-events', 'none');
            setTimeout(function () {
                Site.vars.isAnimating = false;
                $('*').css('pointer-events', 'auto');
            }, duration);
        }
    }

    document.addEventListener('DOMContentLoaded', function () {
        Site.loader();
    });
}());
