'use strict';

var gulp         = require('gulp'),
    sass         = require('gulp-ruby-sass'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglify'),
    babelify     = require('babelify'),
    source       = require('vinyl-source-stream'),
    buffer       = require('vinyl-buffer'),
    browserify   = require('browserify'),
    browserSync  = require('browser-sync').create(),
    sourcemaps   = require('gulp-sourcemaps'),
    clean        = require('gulp-clean'),
    autoprefixer = require('gulp-autoprefixer'),
    include      = require('gulp-include'),
    fileinclude  = require('gulp-file-include'),
    gutil        = require('gulp-util'),
    cleanCSS     = require('gulp-clean-css'),
    htmlmin      = require('gulp-htmlmin'),
    ftp          = require('vinyl-ftp'),
    sassLint     = require('gulp-sass-lint');

/**
 * Default task - 'gulp' command
 */
gulp.task('default', ['serve']);

/**
 * Static Server + watching source files
 */
gulp.task('serve', ['files', 'sass-compile', 'es6'], function() {
    browserSync.init({
        server: {
            baseDir: 'dist'
        }
    });
    gulp.watch('./src/sass/**/*', ['sass-lint', 'sass-compile']);
    gulp.watch(['./src/images/*', './src/*.html', './src/partials/*.html'], ['files']);
    gulp.watch('./src/js/*', ['scripts', 'es6']);
    gulp.watch(['dist/*.html', 'dist/js/*.js']).on('change', function () {
        setTimeout(function () {
            browserSync.reload();
        }, 100);
    });
});

/**
 * Lint sass
 */
 gulp.task('sass-lint', function () {
   return gulp.src(['./src/sass/**/*.scss', '!./src/sass/**/*-nolint.scss'])
     .pipe(sassLint({
       configFile: '.sass-lint.yml'
     }))
     .pipe(sassLint.format())
     .pipe(sassLint.failOnError())
 });

/**
 * Compile with gulp-ruby-sass + source maps
 */
gulp.task('sass-compile', function () {
    return sass('./src/sass/main.scss', {sourcemap: true})
        .on('error', function (err) {
            console.error('Error!', err.message);
        })
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: './src/sass'
        }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

/**
 * Add js scripts
 */
gulp.task('scripts', function() {
    gulp.src(['./src/js/*'])
        // .pipe(sourcemaps.init())
            // .pipe(include())
        // .pipe(sourcemaps.write('./', {
        //     includeContent: false,
        //     sourceRoot: './src/js'
        // }))
        .pipe(gulp.dest('./dist/js'))
});

/**
 * Babelify ES6
 */
gulp.task('es6', () => {
    return browserify('./src/js/main.js')
        .transform('babelify', {loose : 'all'})
        .bundle()
        .pipe(source('main.min.js'))
        .pipe(buffer())
        // .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
});

/**
 * Copy html & other files to dist directory
 */
gulp.task('files', function() {
    gulp.src('./src/*.*')
        .pipe(fileinclude({
          prefix: '@@',
          basepath: '@file'
        }))
        .pipe(gulp.dest('./dist/'));
    gulp.src('./src/images/**/*')
        .pipe(gulp.dest('./dist/images'));
    gulp.src('./src/js/**/*')
        .pipe(gulp.dest('./dist/js'));
        gulp.src('./src/fonts/**/*')
            .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('compress', function () {
    gulp.src('./dist/js/main.js')
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/js'))

    gulp.src('./dist/css/main.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/css'));

    gulp.src('dist/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});

/**
 * Clean dist directory
 */
gulp.task('clean', function() {
    gulp.src(['./dist'])
        .pipe(clean({force : true}));
});

/**
 * Deployment task using vinyl-ftp
 */
gulp.task('deploy', function() {
    var conn = ftp.create( {
        host:     'ftp.kacpergalka.nazwa.pl',
        user:     'kacpergalka_idrinq',
        password: 'cdCbFYvhdF8R',
        parallel: 1,
        log:      gutil.log
    });
    var globs = [
            'dist/**',
    ];
    return gulp.src( globs, { base: './dist', buffer: false } )
        // .pipe( conn.newer( '/' ) ) // only upload newer files
        .pipe( conn.dest( '/' ) );
});
